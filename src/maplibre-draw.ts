import DirectSelect from "./modes/direct_select";
import DrawLineString from "./modes/draw_line_string";
import DrawMultiPolygon from "./modes/draw_multipolygon";
import DrawPoint from "./modes/draw_point";
import DrawPolygon from "./modes/draw_polygon";
import modes from "./modes/index";
import passing_draw_polygon from "./modes/passing_mode_polygon";
import SimpleSelect from "./modes/simple_select";
import SplitPolygonMode from "./modes/split_polygon_mode";
import StaticMode from "./modes/static_mode";
import { setupDraw } from "./setupDraw";

class MaplibreDraw {
  static modes: {
    simple_select: typeof SimpleSelect;
    direct_select: typeof DirectSelect;
    draw_point: typeof DrawPoint;
    draw_polygon: typeof DrawPolygon;
    draw_line_string: typeof DrawLineString;
    static_mode: typeof StaticMode;
    draw_multipolygon: typeof DrawMultiPolygon;
    split_polygon_mode: typeof SplitPolygonMode;
    passing_draw_polygon: typeof passing_draw_polygon;
  };

  constructor(options?) {
    setupDraw(options, this);
  }
}
MaplibreDraw.modes = modes;

export default MaplibreDraw;
