import draw_polygon from "./../modes/draw_polygon";
import * as Constants from "./../constants";
import doubleClickZoom from "./../lib/double_click_zoom";

const {
  onSetup: originOnSetup,
  onMouseMove: originOnMouseMove,
  ...restOriginMethods
} = draw_polygon;

const passing_draw_polygon = {
  originOnSetup,
  originOnMouseMove,
  ...restOriginMethods,
};

passing_draw_polygon.onSetup = function (callBack) {
  const state = this.originOnSetup();
  state.callBack = callBack;
  return state;
};

passing_draw_polygon.onMouseMove = function (state, e) {
  this.updateUIClasses({ mouse: Constants.cursors.ADD });
  this.originOnMouseMove(state, e);
};

passing_draw_polygon.onStop = function (state) {
  const f = state.line || state.polygon;

  this.updateUIClasses({ mouse: Constants.cursors.NONE });
  doubleClickZoom.enable(this);
  this.activateUIButton();
  // check to see if we've deleted this feature
  if (this.getFeature(f.id) === undefined) return;
  //remove last added coordinate
  f.removeCoordinate(`${state.currentVertexPosition}`);
  if (f.isValid()) {
    if (typeof state.callBack === "function") state.callBack(f.toGeoJSON());
    else
      this.map.fire("draw.passing-create", {
        features: [f.toGeoJSON()],
      });
  }
  // else {
  this.deleteFeature([f.id], { silent: true });
  this.changeMode(Constants.modes.SIMPLE_SELECT, {}, { silent: true });
  // }
};

export default passing_draw_polygon;
