import simple_select from "./simple_select";
import direct_select from "./direct_select";
import draw_point from "./draw_point";
import draw_polygon from "./draw_polygon";
import draw_line_string from "./draw_line_string";
import static_mode from "./static_mode";
import draw_multipolygon from "./draw_multipolygon";
import split_polygon_mode from "./split_polygon_mode";
import passing_draw_polygon from "./passing_mode_polygon";

export default {
  simple_select,
  direct_select,
  draw_point,
  draw_polygon,
  draw_line_string,
  static_mode,
  draw_multipolygon,
  split_polygon_mode,
  passing_draw_polygon,
};
