# maplibre-gl-draw by Lightcyphers

# API Reference

To use Draw

```js
// Create a Maplibre GL JS/TS map
var map = new Map(mapOptions);

// Create a Draw control
var draw = new MaplibreDraw(drawOptions);

// Add the Draw control to your map
map.addControl(draw) //if JS
map.addControl(draw as IControl)// if TS
```

**Draw only works after the Mapbox GL JS map has loaded**, so you must interact with Draw only _after_ your map's `load` event:

```js
map.on('load', function() {
  draw.add({ .. });
});


```

cd existing_repo
git remote add origin https://gitlab.com/lightcyphers-open/maplibre/maplibre-gl-draw.git
git branch -M main
git push -uf origin main

```

```
