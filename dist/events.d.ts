export default function _default(ctx: any): {
    start(): void;
    changeMode: (modename: any, nextModeOptions: any, eventOptions?: {}) => void;
    actionable: (actions: any) => void;
    currentModeName(): any;
    currentModeRender(geojson: any, push: any): any;
    fire(name: any, event: any): void;
    addEventListeners(): void;
    removeEventListeners(): void;
    trash(options: any): void;
    combineFeatures(): void;
    uncombineFeatures(): void;
    getMode(): any;
};
