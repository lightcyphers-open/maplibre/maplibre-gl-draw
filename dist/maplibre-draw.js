import modes from "./modes/index";
import { setupDraw } from "./setupDraw";
class MaplibreDraw {
    constructor(options) {
        setupDraw(options, this);
    }
}
MaplibreDraw.modes = modes;
export default MaplibreDraw;
