export default function _default(ctx: any): {
    setActiveButton: (id: any) => void;
    queueMapClasses: (options: any) => void;
    updateMapClasses: () => void;
    clearMapClasses: () => void;
    addButtons: () => HTMLDivElement;
    removeButtons: () => void;
};
