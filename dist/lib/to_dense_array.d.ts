export default toDenseArray;
/**
 * Derive a dense array (no `undefined`s) from a single value or array.
 *
 * @param {any} x
 * @return {Array<any>}
 */
declare function toDenseArray(x: any): Array<any>;
