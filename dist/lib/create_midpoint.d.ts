export default function _default(parent: any, startVertex: any, endVertex: any): {
    type: string;
    properties: {
        meta: string;
        parent: any;
        lng: number;
        lat: number;
        coord_path: any;
    };
    geometry: {
        type: string;
        coordinates: number[];
    };
};
