export default StringSet;
declare class StringSet {
    constructor(items: any);
    _items: {};
    _nums: {};
    _length: any;
    add(x: any): StringSet;
    delete(x: any): StringSet;
    has(x: any): boolean;
    values(): any[];
    clear(): StringSet;
}
