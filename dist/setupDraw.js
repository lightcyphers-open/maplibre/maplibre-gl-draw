import runSetup from "./setup";
import setupOptions from "./options";
import setupAPI from "./api";
import * as Constants from "./constants";
export function setupDraw(options, api) {
    options = setupOptions(options);
    const ctx = {
        options,
    };
    api = setupAPI(ctx, api);
    ctx.api = api;
    const setup = runSetup(ctx);
    api.onAdd = setup.onAdd;
    api.onRemove = setup.onRemove;
    api.types = Constants.types;
    api.options = options;
    return api;
}
