export default Point;
declare class Point extends Feature {
    isValid(): boolean;
    updateCoordinate(pathOrLng: any, lngOrLat: any, lat: any, ...args: any[]): void;
    getCoordinate(): any;
}
import Feature from "./feature";
