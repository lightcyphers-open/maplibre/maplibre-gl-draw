export default MultiFeature;
declare class MultiFeature extends Feature {
    model: any;
    features: any;
    _coordinatesToFeatures(coordinates: any): any;
    isValid(): any;
    getCoordinate(path: any): any;
    updateCoordinate(path: any, lng: any, lat: any): void;
    addCoordinate(path: any, lng: any, lat: any): void;
    removeCoordinate(path: any): void;
    getFeatures(): any;
}
import Feature from "./feature";
