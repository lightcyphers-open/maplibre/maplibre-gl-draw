export default Feature;
declare class Feature {
    constructor(ctx: any, geojson: any);
    ctx: any;
    properties: any;
    coordinates: any;
    id: any;
    type: any;
    changed(): void;
    incomingCoords(coords: any): void;
    setCoordinates(coords: any): void;
    getCoordinates(): any;
    setProperty(property: any, value: any): void;
    toGeoJSON(): any;
    internal(mode: any): {
        type: string;
        properties: {
            id: any;
            meta: string;
            "meta:type": any;
            active: string;
            mode: any;
        };
        geometry: {
            coordinates: any;
            type: any;
        };
    };
}
