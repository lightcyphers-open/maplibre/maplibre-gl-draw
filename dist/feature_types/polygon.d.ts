export default Polygon;
declare class Polygon extends Feature {
    isValid(): any;
    addCoordinate(path: any, lng: any, lat: any): void;
    removeCoordinate(path: any): void;
    getCoordinate(path: any): any;
    updateCoordinate(path: any, lng: any, lat: any): void;
}
import Feature from "./feature";
