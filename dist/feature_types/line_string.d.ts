export default LineString;
declare class LineString extends Feature {
    isValid(): boolean;
    addCoordinate(path: any, lng: any, lat: any): void;
    getCoordinate(path: any): any;
    removeCoordinate(path: any): void;
    updateCoordinate(path: any, lng: any, lat: any): void;
}
import Feature from "./feature";
