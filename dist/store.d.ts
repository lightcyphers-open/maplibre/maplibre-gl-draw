export default class Store {
    constructor(ctx: any);
    _features: {};
    _featureIds: StringSet;
    _selectedFeatureIds: StringSet;
    _selectedCoordinates: any[];
    _changedFeatureIds: StringSet;
    _deletedFeaturesToEmit: any[];
    _emitSelectionChange: boolean;
    _mapInitialConfig: {};
    ctx: any;
    sources: {
        hot: any[];
        cold: any[];
    };
    render: () => void;
    isDirty: boolean;
    /**
     * Delays all rendering until the returned function is invoked
     * @return {Function} renderBatch
     */
    createRenderBatch(): Function;
    /**
     * Sets the store's state to dirty.
     * @return {Store} this
     */
    setDirty(): Store;
    /**
     * Sets a feature's state to changed.
     * @param {string} featureId
     * @return {Store} this
     */
    featureChanged(featureId: string): Store;
    /**
     * Gets the ids of all features currently in changed state.
     * @return {Store} this
     */
    getChangedIds(): Store;
    /**
     * Sets all features to unchanged state.
     * @return {Store} this
     */
    clearChangedIds(): Store;
    /**
     * Gets the ids of all features in the store.
     * @return {Store} this
     */
    getAllIds(): Store;
    /**
     * Adds a feature to the store.
     * @param {Object} feature
     *
     * @return {Store} this
     */
    add(feature: any): Store;
    /**
     * Deletes a feature or array of features from the store.
     * Cleans up after the deletion by deselecting the features.
     * If changes were made, sets the state to the dirty
     * and fires an event.
     * @param {string | Array<string>} featureIds
     * @param {Object} [options]
     * @param {Object} [options.silent] - If `true`, this invocation will not fire an event.
     * @return {Store} this
     */
    delete(featureIds: string | Array<string>, options?: {
        silent?: any;
    }): Store;
    /**
     * Returns a feature in the store matching the specified value.
     * @return {Object | undefined} feature
     */
    get(id: any): any | undefined;
    /**
     * Returns all features in the store.
     * @return {Array<Object>}
     */
    getAll(): Array<any>;
    /**
     * Adds features to the current selection.
     * @param {string | Array<string>} featureIds
     * @param {Object} [options]
     * @param {Object} [options.silent] - If `true`, this invocation will not fire an event.
     * @return {Store} this
     */
    select(featureIds: string | Array<string>, options?: {
        silent?: any;
    }): Store;
    /**
     * Deletes features from the current selection.
     * @param {string | Array<string>} featureIds
     * @param {Object} [options]
     * @param {Object} [options.silent] - If `true`, this invocation will not fire an event.
     * @return {Store} this
     */
    deselect(featureIds: string | Array<string>, options?: {
        silent?: any;
    }): Store;
    /**
     * Clears the current selection.
     * @param {Object} [options]
     * @param {Object} [options.silent] - If `true`, this invocation will not fire an event.
     * @return {Store} this
     */
    clearSelected(options?: {
        silent?: any;
    }): Store;
    /**
     * Sets the store's selection, clearing any prior values.
     * If no feature ids are passed, the store is just cleared.
     * @param {string | Array<string> | undefined} featureIds
     * @param {Object} [options]
     * @param {Object} [options.silent] - If `true`, this invocation will not fire an event.
     * @return {Store} this
     */
    setSelected(featureIds: string | Array<string> | undefined, options?: {
        silent?: any;
    }): Store;
    /**
     * Sets the store's coordinates selection, clearing any prior values.
     * @param {Array<Array<string>>} coordinates
     * @return {Store} this
     */
    setSelectedCoordinates(coordinates: Array<Array<string>>): Store;
    /**
     * Clears the current coordinates selection.
     * @param {Object} [options]
     * @return {Store} this
     */
    clearSelectedCoordinates(): Store;
    /**
     * Returns the ids of features in the current selection.
     * @return {Array<string>} Selected feature ids.
     */
    getSelectedIds(): Array<string>;
    /**
     * Returns features in the current selection.
     * @return {Array<Object>} Selected features.
     */
    getSelected(): Array<any>;
    /**
     * Returns selected coordinates in the currently selected feature.
     * @return {Array<Object>} Selected coordinates.
     */
    getSelectedCoordinates(): Array<any>;
    /**
     * Indicates whether a feature is selected.
     * @param {string} featureId
     * @return {boolean} `true` if the feature is selected, `false` if not.
     */
    isSelected(featureId: string): boolean;
    /**
     * Sets a property on the given feature
     * @param {string} featureId
     * @param {string} property property
     * @param {string} property value
     */
    setFeatureProperty(featureId: string, property: string, value: any): void;
    /**
     * Stores the initial config for a map, so that we can set it again after we're done.
     */
    storeMapConfig(): void;
    /**
     * Restores the initial config for a map, ensuring all is well.
     */
    restoreMapConfig(): void;
    /**
     * Returns the initial state of an interaction setting.
     * @param {string} interaction
     * @return {boolean} `true` if the interaction is enabled, `false` if not.
     * Defaults to `true`. (Todo: include defaults.)
     */
    getInitialConfigValue(interaction: string): boolean;
}
import StringSet from "./lib/string_set";
