export default SplitPolygonMode;
declare namespace SplitPolygonMode {
    function onSetup(): {
        main: any;
    };
    function toDisplayFeatures(state: any, geojson: any, display: any): void;
    function fireUpdate(newF: any): void;
}
