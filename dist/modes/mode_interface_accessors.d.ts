export default class ModeInterface {
    constructor(ctx: any);
    map: any;
    drawConfig: any;
    _ctx: any;
    /**
     * Sets Draw's interal selected state
     * @name this.setSelected
     * @param {DrawFeature[]} - whats selected as a [DrawFeature](https://github.com/mapbox/mapbox-gl-draw/blob/main/src/feature_types/feature.js)
     */
    setSelected(features: any): any;
    /**
     * Sets Draw's internal selected coordinate state
     * @name this.setSelectedCoordinates
     * @param {Object[]} coords - a array of {coord_path: 'string', featureId: 'string'}
     */
    setSelectedCoordinates(coords: any[]): void;
    /**
     * Get all selected features as a [DrawFeature](https://github.com/mapbox/mapbox-gl-draw/blob/main/src/feature_types/feature.js)
     * @name this.getSelected
     * @returns {DrawFeature[]}
     */
    getSelected(): DrawFeature[];
    /**
     * Get the ids of all currently selected features
     * @name this.getSelectedIds
     * @returns {String[]}
     */
    getSelectedIds(): string[];
    /**
     * Check if a feature is selected
     * @name this.isSelected
     * @param {String} id - a feature id
     * @returns {Boolean}
     */
    isSelected(id: string): boolean;
    /**
     * Get a [DrawFeature](https://github.com/mapbox/mapbox-gl-draw/blob/main/src/feature_types/feature.js) by its id
     * @name this.getFeature
     * @param {String} id - a feature id
     * @returns {DrawFeature}
     */
    getFeature(id: string): DrawFeature;
    /**
     * Add a feature to draw's internal selected state
     * @name this.select
     * @param {String} id
     */
    select(id: string): any;
    /**
     * Remove a feature from draw's internal selected state
     * @name this.delete
     * @param {String} id
     */
    deselect(id: string): any;
    /**
     * Delete a feature from draw
     * @name this.deleteFeature
     * @param {String} id - a feature id
     */
    deleteFeature(id: string, opts?: {}): any;
    /**
     * Add a [DrawFeature](https://github.com/mapbox/mapbox-gl-draw/blob/main/src/feature_types/feature.js) to draw.
     * See `this.newFeature` for converting geojson into a DrawFeature
     * @name this.addFeature
     * @param {DrawFeature} feature - the feature to add
     */
    addFeature(feature: DrawFeature): any;
    /**
     * Clear all selected features
     */
    clearSelectedFeatures(): any;
    /**
     * Clear all selected coordinates
     */
    clearSelectedCoordinates(): any;
    /**
     * Indicate if the different action are currently possible with your mode
     * See [draw.actionalbe](https://github.com/mapbox/mapbox-gl-draw/blob/main/API.md#drawactionable) for a list of possible actions. All undefined actions are set to **false** by default
     * @name this.setActionableState
     * @param {Object} actions
     */
    setActionableState(actions?: any): any;
    /**
     * Trigger a mode change
     * @name this.changeMode
     * @param {String} mode - the mode to transition into
     * @param {Object} opts - the options object to pass to the new mode
     * @param {Object} eventOpts - used to control what kind of events are emitted.
     */
    changeMode(mode: string, opts?: any, eventOpts?: any): any;
    /**
     * Update the state of draw map classes
     * @name this.updateUIClasses
     * @param {Object} opts
     */
    updateUIClasses(opts: any): any;
    /**
     * If a name is provided it makes that button active, else if makes all buttons inactive
     * @name this.activateUIButton
     * @param {String?} name - name of the button to make active, leave as undefined to set buttons to be inactive
     */
    activateUIButton(name: string | null): any;
    /**
     * Get the features at the location of an event object or in a bbox
     * @name this.featuresAt
     * @param {Event||NULL} event - a mapbox-gl event object
     * @param {BBOX||NULL} bbox - the area to get features from
     * @param {String} bufferType - is this `click` or `tap` event, defaults to click
     */
    featuresAt(event: any, bbox: any, bufferType?: string): any;
    /**
     * Create a new [DrawFeature](https://github.com/mapbox/mapbox-gl-draw/blob/main/src/feature_types/feature.js) from geojson
     * @name this.newFeature
     * @param {GeoJSONFeature} geojson
     * @returns {DrawFeature}
     */
    newFeature(geojson: GeoJSONFeature): DrawFeature;
    /**
     * Check is an object is an instance of a [DrawFeature](https://github.com/mapbox/mapbox-gl-draw/blob/main/src/feature_types/feature.js)
     * @name this.isInstanceOf
     * @param {String} type - `Point`, `LineString`, `Polygon`, `MultiFeature`
     * @param {Object} feature - the object that needs to be checked
     * @returns {Boolean}
     */
    isInstanceOf(type: string, feature: any): boolean;
    /**
     * Force draw to rerender the feature of the provided id
     * @name this.doRender
     * @param {String} id - a feature id
     */
    doRender(id: string): any;
}
