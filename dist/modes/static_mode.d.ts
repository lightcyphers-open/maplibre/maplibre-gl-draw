export default StaticMode;
declare namespace StaticMode {
    function onSetup(): {};
    function toDisplayFeatures(state: any, geojson: any, display: any): void;
}
