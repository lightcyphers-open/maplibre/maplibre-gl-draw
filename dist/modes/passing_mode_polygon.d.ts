export default passing_draw_polygon;
declare const passing_draw_polygon: {
    clickAnywhere(state: any, e: any): any;
    clickOnVertex(state: any): any;
    onTap: (state: any, e: any) => any;
    onClick(state: any, e: any): any;
    onKeyUp(state: any, e: any): void;
    onStop(state: any): void;
    toDisplayFeatures(state: any, geojson: any, display: any): any;
    onTrash(state: any): void;
    originOnSetup: () => {
        polygon: any;
        currentVertexPosition: number;
    };
    originOnMouseMove: (state: any, e: any) => void;
};
