export default DrawPoint;
declare namespace DrawPoint {
    function onSetup(): {
        point: any;
    };
    function stopDrawingAndRemove(state: any): void;
    function onTap(state: any, e: any): void;
    function onClick(state: any, e: any): void;
    function onStop(state: any): void;
    function toDisplayFeatures(state: any, geojson: any, display: any): any;
    function onKeyUp(state: any, e: any): void;
}
