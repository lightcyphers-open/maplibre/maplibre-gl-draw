export default function _default(ctx: any): {
    onRemove(): any;
    connect(): void;
    onAdd(map: any): any;
    addLayers(): void;
    removeLayers(): void;
};
